#pragma once
#include <string>
#include "Engine_Data.h"
#include "IEngine_DataToView.h"
#include <Windows.h>

class CEngine_View
{
private:
	bool			m_bMapActive;
public:
	CEngine_View(void);
	virtual ~CEngine_View(void);
	//Initialize & Finalize
	virtual bool Initialize(std::string& error);
	virtual bool Finalize(std::string& error);
	//Operations
	bool	Render(HDC& hDC,int nWidth,int nHeight,const IEngine_DataToView* data);
	void	ToggleMap()					{m_bMapActive = !m_bMapActive;}
};