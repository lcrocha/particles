// Particles.cpp : Defines the entry point for the application.
//

#include "main.h"
#include <stdio.h>
#include "Engine_Processor.h"
#include "Engine_Data.h"
#include "Engine_View.h"

#define MAX_LOADSTRING 100

#define WM_IDLE			WM_USER + 1
#define WM_FINISHED		WM_USER + 2

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

CEngine_Processor	g_processor;
CEngine_Data		g_data;
CEngine_View		g_view;
bool				g_update;
bool				g_running;

struct BACKBUFFER
{
	bool	initialized;
	HDC		hDC;
	HBITMAP	hBitmap;
	RECT	rc;
}g_backbuffer;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int,HWND&);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int CALLBACK WinMain(
    _In_  HINSTANCE hInstance,
    _In_  HINSTANCE hPrevInstance,
    _In_  LPSTR lpCmdLine,
    _In_  int nCmdShow
  )
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	HWND hWnd = 0L;
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	::LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	::LoadString(hInstance, IDC_PARTICLES, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);
	int n = ::GetTickCount();
	srand(n);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow,hWnd))
	{
		return FALSE;
	}

	hAccelTable = ::LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PARTICLES));

	// Main message loop:
	while(true)
	{
		if(::PeekMessage(&msg,nullptr,0,0,PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
			{
				break;
			}
			else if (!::TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
			{
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}
		}
		else if(g_running)
		{
			//Idle
			::PostMessage(hWnd,WM_IDLE,0L,0L);
			::Sleep(1);
		}
		else
		{
			::Sleep(50);
		}
	}

	//Finalizing engines
	std::string error;
	if(!g_data.Finalize(error))
	{
		::MessageBoxA(nullptr, error.c_str(), "Error!", MB_OK);
	}

	if(!g_view.Finalize(error))
	{
		::MessageBoxA(nullptr, error.c_str(), "Error!", MB_OK);
	}

	if(!g_processor.Finalize(error))
	{
		::MessageBoxA(0L,error.c_str(),"Error!",MB_OK);
	}

	return static_cast<int> (msg.wParam);
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PARTICLES));
	wcex.hCursor		= LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_PARTICLES);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= ::LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return ::RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow,HWND& hWnd)
{
	BOOL ret = FALSE;

	hInst = hInstance; // Store instance handle in our global variable

	hWnd = ::CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, 600/*508*/, 494, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	//Initializing engines
	std::string error;
	if(!g_data.Initialize(error))
	{
		::MessageBoxA(0L,error.c_str(),"Error!",MB_OK);
		goto exit;
	}

	if(!g_view.Initialize(error))
	{
		::MessageBoxA(0L,error.c_str(),"Error!",MB_OK);
		goto exit;
	}

	if(!g_processor.Initialize(error))
	{
		::MessageBoxA(0L,error.c_str(),"Error!",MB_OK);
		goto exit;
	}

	g_processor.DoStatistics(g_data);
	::ShowWindow(hWnd, nCmdShow);
	::UpdateWindow(hWnd);
	ret = TRUE;

exit:
	return ret;
}

static void lDoTurn(HWND hWnd)
{
	static DWORD first = ::GetTickCount();
	auto timeElapsed = ::GetTickCount() - first;
	//Do turn
	g_processor.DoTurn(g_data, timeElapsed);
	g_processor.DoStatistics(g_data);
	g_update = true;
	::InvalidateRect(hWnd,nullptr,FALSE);
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_CREATE:
		memset(&g_backbuffer,0,sizeof(g_backbuffer));
		g_update = true;
		g_running = true;
		break;
	case WM_KEYDOWN:
		{
			switch(wParam)
			{
			case VK_SPACE:
				{
					g_running = !g_running;
				}
				break;
			case 's':
			case 'S':
				{
					//Persist data
					std::string error;
					if(!g_data.PersistData(error))
					{
						MessageBoxA(0L,error.c_str(),"Error!",MB_OK);
					}
					else
					{
						MessageBoxA(0L,"Saved!","Particles",MB_OK);
					}
				}
				break;
			case 'm':
			case 'M':
				//Toggle map
				g_view.ToggleMap();
				g_update = true;
				::InvalidateRect(hWnd,nullptr,FALSE);
				break;
			}
		}
		break;
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			::DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			::DestroyWindow(hWnd);
			break;
		default:
			return ::DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_SIZE:
		{
			if (g_backbuffer.initialized)
			{
				if (g_backbuffer.hDC != 0L)
				{
					::ReleaseDC(hWnd,g_backbuffer.hDC);
					g_backbuffer.hDC = 0L;
				}
				if (g_backbuffer.hBitmap != 0L)
				{
					::DeleteObject(g_backbuffer.hBitmap);
					g_backbuffer.hBitmap = 0L;
				}
				g_backbuffer.initialized = false;
			}
		}
		break;
	case WM_PAINT:
		{
			hdc = ::BeginPaint(hWnd, &ps);
			//Initializing backbuffer
			if(!g_backbuffer.initialized)
			{
				int width = 0;
				int height = 0;
				//Setting values
				GetClientRect(hWnd,&g_backbuffer.rc);
				width = g_backbuffer.rc.right - g_backbuffer.rc.left;
				height = g_backbuffer.rc.bottom - g_backbuffer.rc.top;
				/*if(width > height)
				{
					height = width;
				}
				else
				{
					width = height;
				}*/
				//Creating DC
				g_backbuffer.hDC = CreateCompatibleDC(hdc);
				//Bitmap
				g_backbuffer.hBitmap = CreateCompatibleBitmap(hdc,width,height);
				//All done
				::SelectObject(g_backbuffer.hDC,g_backbuffer.hBitmap);
				g_backbuffer.initialized = true;
			}
			//Updating
			int width = g_backbuffer.rc.right - g_backbuffer.rc.left;
			int height = g_backbuffer.rc.bottom - g_backbuffer.rc.top;
			if(g_update)
			{
				//Rendering
#ifdef _DEBUG
				DWORD first = ::GetTickCount();
#endif
				if(!g_view.Render(g_backbuffer.hDC,width,height,&g_data))
				{
					g_running = false;
					::PostMessage(hWnd,WM_FINISHED,0L,0L);
				}
#ifdef _DEBUG
				auto total = ::GetTickCount() - first;
#endif
				g_update = false;
			}
			//Copy buffer to front
			::BitBlt(hdc, g_backbuffer.rc.left, g_backbuffer.rc.top, width, height, g_backbuffer.hDC, 0, 0, SRCCOPY);
			//Finalizing
			::EndPaint(hWnd, &ps);
		}
		break;
	case WM_DESTROY:
		::DeleteObject(g_backbuffer.hBitmap);
		::DeleteDC(g_backbuffer.hDC);
		::PostQuitMessage(0);
		break;
	case WM_FINISHED:
		{
			std::string report;
			g_data.GenerateReport(report);
			::MessageBoxA(hWnd,report.c_str(),"Particles",0L);
			::PostMessage(hWnd,WM_DESTROY,0L,0L);
		}
		break;
	case WM_IDLE:
		{
#ifdef _DEBUG
			auto first = ::GetTickCount();
#endif
			lDoTurn(hWnd);
#ifdef _DEBUG
			auto total = ::GetTickCount() - first;
#endif
		}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			::EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
