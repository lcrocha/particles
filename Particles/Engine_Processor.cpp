#include "Engine_Processor.h"
#include <algorithm>
#include <sstream>

#define SAFE_DELETE(p) {if(p != nullptr){ delete p; p = nullptr;}}

typedef std::vector<MAP_CELL*> MAP_CELLS;
struct PROCESSING_CELL
{
	MAP_CELL*	Cell;
	MAP_CELLS	AdjacentCells;
};
typedef std::vector<PROCESSING_CELL> PROCESSING_CELLS;

template<typename T>
std::string ToString(const T& t)
{
	std::ostringstream s;
	s << t;
	return s.str();
}

CEngine_Processor::CEngine_Processor(void)
{
}


CEngine_Processor::~CEngine_Processor(void)
{
}

bool CEngine_Processor::Initialize(std::string& error)
{
	bool ret = true;

	return ret;
}

bool CEngine_Processor::Finalize(std::string& error)
{
	auto ret = true;

	return ret;
}

void lGetAdjacentCells(CEngine_Data& data, int reti, int retj, MAP_CELLS& adjacentCells, bool randomize)
{
	auto max = data.GetMapSize();
	for(auto i = -1;i <= 1;i++)
	{
		auto a = reti + i;
		if(a < 0)
		{
			a += max;
		}
		else if(a >= max)
		{
			a -= max;
		}
		if(a >= 0 && a < max)
		{
			for(auto j = -1;j <= 1;j++)
			{
				auto b = retj + j;
				if(b < 0)
				{
					b += max;
				}
				else if(b >= max)
				{
					b -= max;
				}
				if((b >= 0) && (b < max) && ((retj != b) || (reti != a)))
				{
					adjacentCells.push_back(&data.GetCell(a, b));
				}
			}
		}
	}
	//Shuffling
	if(randomize)
	{
		std::random_shuffle(adjacentCells.begin(), adjacentCells.end());
	}
}

static float lRandNumber()
{
	auto ret = 0.0f;
	auto r = rand() % 1000;
	if(r == 0)
	{
		ret = 0.01f;
	}
	else if(r == 999)
	{
		ret = -0.01f;
	}

	return ret;
}

static void lIncrementStastistics(STATISTICS& stats,float attack,float speed,float defense)
{
	PARTICLE_STATISTICS::iterator it = std::find_if(stats.Particles.begin(), stats.Particles.end(), [&](const PARTICLE_STATISTIC& s)
	{
		return (attack == s.Attack) && (defense == s.Defense) && (speed == s.Speed);
	});
	if (it == stats.Particles.end())
	{
		PARTICLE_STATISTIC s(attack,speed,defense);
		s.Count = 1;
		s.Turns = 0;
		stats.Particles.push_back(s);
	}
	else
	{
		it->Count++;
	}
	//Testing max
	stats.MaxSpeed = speed > stats.MaxSpeed ? speed : stats.MaxSpeed;
	stats.MaxAttack = attack > stats.MaxAttack ? attack : stats.MaxAttack;
	stats.MaxDefense = defense > stats.MaxDefense ? defense : stats.MaxDefense;
}

float lCalculateAutotrophism(Particle* p)
{
	auto factor = 1.0f / (1.0f + p->GetAttributes().Attack);

	return factor;
}

static float lCalculateHeterotrophism(Particle* p)
{
	return 1.0f - lCalculateAutotrophism(p);
}

static float lCalculateAttackFactor(Particle* p)
{
	return p->GetAttributes().Attack + 0.1f*p->GetAttributes().Speed;
}

static float lCalculateDefenseFactor(Particle* p)
{
	return p->GetAttributes().Defense + 0.1f*p->GetAttributes().Speed;
}

static bool lCheckParticleSpecies(Particle* current, Particle* adjacent)
{
	auto& currentAttr = current->GetAttributes();
	auto& adjacentAttr = adjacent->GetAttributes();

	return currentAttr.Attack == adjacentAttr.Attack &&
		currentAttr.Speed == adjacentAttr.Speed &&
		currentAttr.Defense == adjacentAttr.Defense;
}

float lCalculateCurrentDeltaEnergy(Particle* current, Particle* adjacent, float energyMax)
{
	auto ret = 0.0f;
	if (current != nullptr && adjacent != nullptr && !lCheckParticleSpecies(current, adjacent))
	{
		//Attacking
		if (lCalculateAttackFactor(current) > lCalculateDefenseFactor(adjacent))
		{
			auto e = energyMax * lCalculateHeterotrophism(current);
			if (e > adjacent->GetAttributes().Energy)
			{
				e = adjacent->GetAttributes().Energy;
			}
			auto currentEnergySpace = energyMax - current->GetAttributes().Energy;
			ret += (e > currentEnergySpace ? currentEnergySpace : e);
		}
		//Loosing
		if (lCalculateAttackFactor(adjacent) > lCalculateDefenseFactor(current))
		{
			auto e = energyMax * lCalculateHeterotrophism(adjacent);
			if (e > current->GetAttributes().Energy)
			{
				e = current->GetAttributes().Energy;
			}
			auto adjacentEnergySpace = energyMax - adjacent->GetAttributes().Energy;
			ret -= (e > adjacentEnergySpace ? adjacentEnergySpace : e);
		}
	}

	return ret;
}

static bool lCheckMovement(float& movementGain, Particle* p, float maxEnergy)
{
	auto ret = false;
	//Getting factor
	auto accumulatedSpeed = p->GetAttributes().AccmulatedSpeed;
	auto factor = 1.0f - 1.0f / (1.0f + accumulatedSpeed);
	auto number = 0.01f * ((rand() % 99) + 1);
	if (factor > number)
	{
		//movementPenalty = 0.1f*maxEnergy / (1.0f + accumulatedSpeed);
		movementGain = 0.02f*maxEnergy*(1.0f - (1.0f/(1.0f + accumulatedSpeed)));
		auto energy = p->GetAttributes().Energy;
		ret = true;
	}

	return ret;
}

float lCalculateCurrentTurnRulesEnergy(Particle* particle, int lifePenalty)
{
	auto ret = 0.0f;
	float attack = particle->GetAttributes().Attack;
	float speed = particle->GetAttributes().Speed;
	float defense = particle->GetAttributes().Defense;
	//Apply turns rule
	if (lifePenalty > 0)
	{
		//Apply attributes decay
		ret -= (2.0f*attack + 1.5f*defense + 1.0f*speed);
		ret -= static_cast<float>(particle->GetAttributes().Turns++) / lifePenalty;
	}

	return ret;
}

static float lAdjustingBaseEnergy(int i, int j, int mapSize, long totalTime, float baseMax, float baseFactor)
{
	auto ret = 0.0f;
	auto pi = 4.0f*atanf(1.0f);
	auto relX = static_cast<float>(i) / static_cast<float>(mapSize - 1);
	auto relY = static_cast<float>(j) / static_cast<float>(mapSize - 1);
	//auto val = 1.0f*cosf(relY*2.0f*pi + 0.0001f*totalTime);
	//auto val = sinf(relY*2.0f*pi + 0.00000000f*totalTime - 0.5f*pi) + 0.85f;
	auto x = cosf(relX*2.0f*pi - 1.0f*pi) + 0.97f;
	auto y = sinf(relY*2.0f*pi - 0.5f*pi + 0.0000001f*totalTime) + 0.9f;
	auto val = 3.0f*x + 1.1f*y;
	//auto val = 0.05f*cosf(relY*2.0f*pi + 0.0001f*time) + 0.05f*sinf(relX*2.0f*pi + 0.0001f*time);
	//auto val = 1.0f*fabs(co sf(2.0f*pi + 0.001f*time));
	//auto val = 1.0f*0.05f*(time);
	ret += baseFactor*val;// > 0 ? val : 0;
	//Validating base energy
	if (ret < 0)
	{
		ret = 0;
	}
	else if (ret > baseMax)
	{
		ret = baseMax;
	}

	return ret;
}

static bool lApplyDeathRules(CEngine_Data& data, MAP_CELL* cell)
{
	auto ret = false;
	if (data.ViewConfig().EnergyMax > 0 && cell->Particle != nullptr && cell->Particle->GetAttributes().Energy <= 0.0f)
	{
		data.GetStatistics().Deaths++;
		SAFE_DELETE(cell->Particle);
		ret = true;
	}

	return ret;
}

static bool lApplyReplicationRules(CEngine_Data& data, Particle* currentParticle, MAP_CELL* availableCell)
{
	auto ret = false;
	auto energyMax = data.ViewConfig().EnergyMax;
	if (energyMax > 0 && currentParticle->GetAttributes().Energy >= energyMax)
	{
		//Getting current attributes
		auto attack = currentParticle->GetAttributes().Attack;
		auto defense = currentParticle->GetAttributes().Defense;
		auto speed = currentParticle->GetAttributes().Speed;
		auto energy = currentParticle->GetAttributes().Energy;
		//Creating new particle
		auto newparticle = new Particle();
		auto newname = currentParticle->GetName() + std::string(".") + ToString<int>(currentParticle->GetAttributes().Replicated++);
		auto newEnergy = 0.5f*energy;
		newparticle->SetName(newname);
		//Calculating mutation
		newparticle->GetAttributes().Attack.SetValue(attack + lRandNumber());
		newparticle->GetAttributes().Speed.SetValue(speed + lRandNumber());
		newparticle->GetAttributes().Defense.SetValue(defense + lRandNumber());
		newparticle->GetAttributes().Energy.SetValue(newEnergy);
		newparticle->GetAttributes().Replicated = 1;
		newparticle->GetAttributes().Turns = 0;
		availableCell->Particle = newparticle;
		//Setting fathers data
		currentParticle->GetAttributes().Energy.SubtractValue(newEnergy);
		data.GetStatistics().Replications++;
		ret = true;
	}

	return ret;
}

static bool lApplyMovementRules(const CONFIG& config, PROCESSING_CELL& processingCell, PROCESSING_CELLS& availableCells)
{
	auto ret = false;
	//Adding speed
	auto cell = processingCell.Cell;
	auto& adjacentCells = processingCell.AdjacentCells;
	auto current = cell->Particle;
	auto speed = current->GetAttributes().Speed;
	current->GetAttributes().AccmulatedSpeed.AddValue(speed);
	//Getting movement penalty
	auto energyMax = config.EnergyMax;
	auto movementGain = 0.0f;
	if (lCheckMovement(movementGain, current, energyMax))
	{
		auto autotrophism = lCalculateAutotrophism(current);
		auto bestGain = cell->Base * autotrophism;
		//Calculating actual gain
		std::for_each(adjacentCells.begin(), adjacentCells.end(), [&](MAP_CELL* adjacentCell)
		{
			auto adjacentParticle = adjacentCell->Particle;
			if (adjacentParticle != nullptr)
			{
				bestGain +=
					lCalculateCurrentDeltaEnergy(current, adjacentParticle, energyMax);
			}
		});
		//Selecting the best movement
		auto bestCell = cell;
		std::for_each(availableCells.begin(), availableCells.end(), [&](const PROCESSING_CELL& availableCell)
		{
			auto adjacentGain = static_cast<float>(availableCell.Cell->Base * autotrophism) + movementGain;
			std::for_each(availableCell.AdjacentCells.begin(), availableCell.AdjacentCells.end(), [&](MAP_CELL* adjacentCell)
			{
				if (cell != adjacentCell && adjacentCell->Particle != nullptr)
				{
					adjacentGain += lCalculateCurrentDeltaEnergy(current, adjacentCell->Particle, energyMax);
				}
			});
			if (adjacentGain > bestGain)
			{
				bestGain = adjacentGain;
				bestCell = availableCell.Cell;
			}

		});
		//Testing movement
		if (bestCell != cell)
		{
			//Moving cell
			bestCell->Particle = current;
			cell->Particle = nullptr;
			current->GetAttributes().Energy.AddValue(movementGain);
			current->GetAttributes().AccmulatedSpeed.SetMin();
			ret = true;
		}
	}

	return ret;
}
static bool lGetAvailableCells(CEngine_Data& data, MAP_CELLS& adjacentCells, PROCESSING_CELLS& availableCells)
{
	auto ret = false;
	//Getting available cell
	std::for_each(adjacentCells.begin(), adjacentCells.end(), [&](MAP_CELL* c)
	{
		if (c->Particle == nullptr)
		{
			PROCESSING_CELL availableCell;
			lGetAdjacentCells(data, c->i, c->j, availableCell.AdjacentCells, true);
			availableCell.Cell = c;
			availableCells.push_back(availableCell);
		}
	});

	return !availableCells.empty();
}

static bool lApplyPostRules(CEngine_Data& data, PROCESSING_CELLS& processingCells)
{
	auto ret = false;
	std::for_each(processingCells.begin(), processingCells.end(), [&](PROCESSING_CELL& processingCell)
	{
		auto& cell = processingCell.Cell;
		auto& current = cell->Particle;
		auto energyMax = data.ViewConfig().EnergyMax;
		auto& adjacentCells = processingCell.AdjacentCells;
		//Testing death
		if (!lApplyDeathRules(data, cell))
		{
			//Getting available cell
			PROCESSING_CELLS availableCells;
			if(lGetAvailableCells(data, adjacentCells, availableCells))
			{
				//Testing replication
				ret |= lApplyReplicationRules(data, current, availableCells[0].Cell) ||
					lApplyMovementRules(data.ViewConfig(), processingCell, availableCells);
			}
		}
		//Adjusting energy
		if (cell->Particle != nullptr)
		{
			current = cell->Particle;
			if (energyMax > 0 && current->GetAttributes().Energy > energyMax)
			{
				current->GetAttributes().Energy.SetValue(energyMax);
			}
			else if (current->GetAttributes().Energy < 0)
			{
				current->GetAttributes().Energy.SetMin();
			}
		}
	});

	return ret;
}

static void lApplyHeterotrophismRules(float energyMax, PROCESSING_CELLS& processingCells)
{
	if (energyMax > 0)
	{
		std::for_each(processingCells.begin(), processingCells.end(), [&](PROCESSING_CELL& processingCell)
		{
			auto& cell = processingCell.Cell;
			auto& currentParticle = cell->Particle;
			auto& adjacentCells = processingCell.AdjacentCells;
			//Apply heteretrophism rules
			std::for_each(adjacentCells.begin(), adjacentCells.end(), [&](MAP_CELL* adjacentCell)
			{
				auto adjacentParticle = adjacentCell->Particle;
				if (adjacentParticle != nullptr)
				{
					auto delta = lCalculateCurrentDeltaEnergy(currentParticle, adjacentParticle, energyMax);
					if (delta != 0.0f)
					{
						currentParticle->GetAttributes().Energy.AddValue(delta);
						adjacentParticle->GetAttributes().Energy.SubtractValue(delta);
					}
				}
			});
		});
	}
}

static void lApplyAutotrophismRules(const CONFIG& config, MAP_CELL& cell)
{
	if (config.EnergyMax > 0)
	{
		auto p = cell.Particle;
		auto baseEnergy = cell.Base * lCalculateAutotrophism(p);
		cell.Base -= baseEnergy;
		p->GetAttributes().Energy.AddValue(baseEnergy + lCalculateCurrentTurnRulesEnergy(p, config.LifePenalty));
	}
	else
	{
		cell.Base = 0;
	}
}

static bool lApplyBasicRulse(CEngine_Data& data, PROCESSING_CELLS& processingCells, int totalTime)
{
	auto& config = data.ViewConfig();
	auto& map = data.GetMap();
	auto mapSize = data.GetMapSize();
	//Browsing all cells
	std::for_each(map.begin(), map.end(), [&](MAP_CELL& cell)
	{
		cell.Base = lAdjustingBaseEnergy(cell.i, cell.j, mapSize, totalTime, config.BaseMax, config.BaseFactor);
		//checking particle
		auto current = cell.Particle;
		if (current != nullptr)
		{
			//Apply autotrophism rules
			lApplyAutotrophismRules(config, cell);
			//Getting adjacentCells
			PROCESSING_CELL processingCell;
			processingCell.Cell = &cell;
			lGetAdjacentCells(data, cell.i, cell.j, processingCell.AdjacentCells, true);
			//Adding particles to processing cells
			processingCells.push_back(processingCell);
		}
	});

	return !processingCells.empty();
}

bool CEngine_Processor::DoTurn(CEngine_Data& data, long time)
{
	auto ret = true;
	static auto totalTime = 0;
	auto map = data.GetMap();
	auto config = data.ViewConfig();
	auto energyMax = config.EnergyMax;
	//Getting processing cells
	PROCESSING_CELLS processingCells;
	if (lApplyBasicRulse(data, processingCells, totalTime))
	{
		//Applying particle damages
		lApplyHeterotrophismRules(energyMax, processingCells);
		//Applying movement, replication or death
		lApplyPostRules(data, processingCells);
	}
	//Adjusting time
	totalTime += time;
	//Finalizing turn
	data.IncrementTurn();

	return ret;
}

bool CEngine_Processor::DoStatistics(CEngine_Data& data)
{
	bool ret = false;
	//Cleaning statistics
	std::for_each(data.GetStatistics().Particles.begin(),data.GetStatistics().Particles.end(),[&](PARTICLE_STATISTIC& s)
	{
		s.Count = 0;
	});
	//Counting particles
	std::for_each(data.GetMap().begin(),data.GetMap().end(),[&](MAP_CELL& currentMapCell)
	//Concurrency::parallel_for_each(data.GetMap().begin(),data.GetMap().end(),[&](MAP_CELL& currentMapCell)
	{
		auto currentParticle = currentMapCell.Particle;
		if(currentParticle != nullptr)
		{
			ret = true;
			//Applying statistics
			lIncrementStastistics(
				data.GetStatistics(),
				currentParticle->GetAttributes().Attack,
				currentParticle->GetAttributes().Speed,
				currentParticle->GetAttributes().Defense);
		}
	});
	if(ret)
	{
		//Sorting statistics
		std::sort(data.GetStatistics().Particles.begin(),data.GetStatistics().Particles.end(),[&](const PARTICLE_STATISTIC& s1,const PARTICLE_STATISTIC& s2)
		{
			return s1.Count > s2.Count;
		});
		//Increment turns
		std::for_each(data.GetStatistics().Particles.begin(),data.GetStatistics().Particles.end(),[&](PARTICLE_STATISTIC& s)
		{
			if(s.Count > 0)
			{
				s.Turns++;
			}
		});
	}

	return ret;
}