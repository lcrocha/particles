#pragma once
#include <string>

template<class eType>
class DelimitedAttribute
{
private:
	eType _value;
	eType _min;
	eType _max;
public:
	explicit DelimitedAttribute(eType min) : _value(min), _min(min), _max(min) {}
	DelimitedAttribute(eType min, eType max) : _value(min), _min(min), _max(max) {}
	DelimitedAttribute(eType min, eType max, eType value) : _value(min), _min(min), _max(max)
	{
		SetValue(value);
	}
	//Set's
	void SetMin()	{ _value = _min; }
	void SetMax()	{ _value = _max; }
	void SetValue(eType value)
	{
		if (value > _min)
		{
			if (_max == _min || (_max >= value))
			{
				_value = value;
			}
		}
		else
		{
			SetMin();
		}
	}
	//Operations
	void AddValue(eType value)
	{
		SetValue(_value + value);
	}

	void SubtractValue(eType value)
	{
		SetValue(_value - value);
	}
	//Get
	//eType GetValue() { return _value; }
	//Properties
	//__declspec(property(get = GetValue/*, put = SetValue*/)) eType Value;
	operator const eType()
	{
		return _value;
	}
};

struct PARTICLE_ATTRIBUTES
{
	DelimitedAttribute<float> Attack;
	DelimitedAttribute<float> Defense;
	DelimitedAttribute<float> Speed;
	DelimitedAttribute<float> AccmulatedSpeed;
	DelimitedAttribute<float> Energy;
	int			Replicated;
	int			Turns;
	PARTICLE_ATTRIBUTES(): 
		Attack(0.0f),
		Defense(0.0f),
		Speed(0.0f),
		AccmulatedSpeed(0.0f),
		Energy(0.0f),
		Replicated(0),
		Turns(0)
	{
	}
};

class Particle
{
private:
	std::string				m_name;
	PARTICLE_ATTRIBUTES		m_attributes;
public:
	Particle();
	virtual ~Particle(void);
	//Gets
	const std::string&			GetName()					{return m_name;}
	PARTICLE_ATTRIBUTES&		GetAttributes()				{return m_attributes;}
	//Sets
	void						SetName(std::string& name)	{m_name = name;}
};