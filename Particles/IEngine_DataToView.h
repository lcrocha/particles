#pragma once

struct PARTICLE_STATISTIC
{
	int		Count;
	int		Turns;
	float	Attack;
	float	Speed;
	float	Defense;
	PARTICLE_STATISTIC()
	{
		Attack		= 0.0f;
		Speed		= 0.0f;
		Defense		= 0.0f;
		Count		= 0;
		Turns		= 0;
	}
	PARTICLE_STATISTIC(float attack,float speed,float defense)
	{
		Attack		= attack;
		Speed		= speed;
		Defense		= defense;
		Count		= 0;
		Turns		= 0;
	}
};

typedef std::vector<PARTICLE_STATISTIC>		PARTICLE_STATISTICS;

struct STATISTICS
{
	int						Turns;
	int						Replications;
	int						Deaths;
	float					MaxSpeed;
	float					MaxDefense;
	float					MaxAttack;
	PARTICLE_STATISTICS		Particles;
	STATISTICS() : Turns(0), Replications(0), Deaths(0), MaxSpeed(0.0f), MaxDefense(0.0f), MaxAttack(0.0f) {}
};

typedef std::vector<Particle*>		ADJACENT_PARTICLES;

struct MAP_CELL
{
	int						i;
	int						j;
	float					Base;
	Particle*				Particle;

	bool operator < (const MAP_CELL& cell) const
	{
		auto ret = false;
		if (i < cell.i)
		{
			ret = true;
		}
		else if (i == cell.i)
		{
			ret = (j < cell.j);
		}

		return ret;
	}
};
typedef std::vector<MAP_CELL>	MAP;

struct CONFIG
{
	int		LifePenalty;
	float	EnergyMax;
	float	BaseMax;
	float	BaseFactor;
};

class IEngine_DataToView
{
public:
	//Get
	virtual const CONFIG&		ViewConfig() const = 0;
	virtual const MAP&			ViewMap() const = 0;
	virtual const STATISTICS&	ViewStatistics() const = 0;
	virtual const int			GetMapSize() const = 0;
};