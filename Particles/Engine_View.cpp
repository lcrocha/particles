#include <algorithm>
#include "Engine_View.h"
#include <sstream>
#include <iomanip>
#include <math.h>

#define ASSERT_ZERO(n)	static_cast<float>(n < 0.1f ? 0.0f : n)
#define ASSERT_MAX(n,m)	(n > m ? m : n)

CEngine_View::CEngine_View(void) : m_bMapActive(true)
{
}


CEngine_View::~CEngine_View(void)
{
}

bool CEngine_View::Initialize(std::string& error)
{
	auto ret = true;

	return ret;
}

bool CEngine_View::Finalize(std::string& error)
{
	auto ret = true;

	return ret;
}

static int lGetParticleColor(float attack, float speed, float defense, float maxAttack, float maxSpeed, float maxDefense, float maxEnergy)
{
	return maxEnergy > 0 ?
		RGB(attack * 255 / maxAttack, speed * 255 / maxSpeed, defense * 255 / maxDefense) :
		RGB(255, 255, 255);
}

void DrawSummary(HDC& hDC, int width, int height, const IEngine_DataToView* data, int totalParticles)
{
	//Summarizing active particles
	unsigned int activeParticles = 0;
	std::for_each(data->ViewStatistics().Particles.begin(), data->ViewStatistics().Particles.end(), [&](const PARTICLE_STATISTIC& s)
	{
		if (s.Count > 0)
		{
			activeParticles++;
		}
	});
	//Drawing legend canvas
	RECT canvas = { height, 0, width, height };
	{
		auto brush = ::CreateSolidBrush(RGB(80, 80, 80));
		auto pen = ::CreatePen(PS_SOLID, 1, RGB(80, 80, 80));
		//Selecting objects
		auto oldbrush = ::SelectObject(hDC, brush);
		auto oldpen = ::SelectObject(hDC, pen);
		//Printing canvas
		::FillRect(hDC, &canvas, brush);
		//Returning objects
		::SelectObject(hDC, oldbrush);
		::SelectObject(hDC, oldpen);
		//Deleting objects
		::DeleteObject(pen);
		::DeleteObject(brush);
	}
	//Draw particle summary and pie graph
	std::ostringstream oss;
	auto size = 15;
	unsigned int line = 0;
	//::SetBkColor(hDC,RGB(0,0,0));
	::SetBkMode(hDC, TRANSPARENT);
	::SetTextColor(hDC, RGB(255, 255, 255));
	{
		auto rc = canvas;
		rc.bottom = 3*size;
		unsigned int lineMax = 10;
		auto partialTotal = 0;
		auto centerX = canvas.left + (canvas.right - canvas.left) / 2;
		auto centerY = 270;
		auto maxAttack = data->ViewStatistics().MaxAttack > 0 ? data->ViewStatistics().MaxAttack : 1;
		auto maxDefense = data->ViewStatistics().MaxDefense > 0 ? data->ViewStatistics().MaxDefense : 1;
		auto maxSpeed = data->ViewStatistics().MaxSpeed > 0 ? data->ViewStatistics().MaxSpeed : 1;
		auto maxEnergy = data->ViewConfig().EnergyMax;
		auto LastAngle = 0.0f;
		auto radius = static_cast<int>(0.4f*(canvas.right - canvas.left));

		std::for_each(data->ViewStatistics().Particles.begin(), data->ViewStatistics().Particles.end(), [&](const PARTICLE_STATISTIC& s)
		{
			//Printing report
			if (line < lineMax && s.Count > 0)
			{
				RECT rcParticle = { 0 };
				auto totalLocal = s.Count;
				if ((line == lineMax - 1) && (activeParticles > lineMax))
				{
					totalLocal = totalParticles - partialTotal;
				}
				else
				{
					partialTotal += totalLocal;
				}
				//Setting line rect
				rc.top = rc.bottom;
				rc.bottom += size;
				oss << "    - " << std::setw(3) << totalLocal;
				if (line == lineMax - 1 && (activeParticles > lineMax))
				{
					oss << " (x,x,x)";
				}
				else
				{
					oss << " ("
						<< std::setprecision(2) << s.Attack << ","
						<< s.Speed << ","
						<< s.Defense << ")";
				}
				::DrawTextA(hDC, oss.str().c_str(), oss.str().size(), &rc, DT_LEFT | DT_VCENTER);
				oss.str("");
				//Creating GDIObjects
				auto color = RGB(125, 125, 125);
				HBRUSH brushparticle = nullptr;
				HPEN penparticle = nullptr;
				//Calculating color
				if ((line != lineMax - 1) || (activeParticles <= lineMax))
				{
					color = lGetParticleColor(s.Attack, s.Speed, s.Defense, maxAttack, maxSpeed, maxDefense, maxEnergy);
					brushparticle = ::CreateSolidBrush(color);
					penparticle = ::CreatePen(PS_SOLID, 1, color);
				}
				else
				{
					brushparticle = ::CreateHatchBrush(HS_CROSS, color);
					penparticle = ::CreatePen(PS_SOLID, 1, color);
				}
				//Selecting object
				auto oldbrush = ::SelectObject(hDC, brushparticle);
				auto oldpen = ::SelectObject(hDC, penparticle);
				//Setting particle
				::SetRect(&rcParticle, rc.left + 1, rc.top + 1, rc.left + size - 1, rc.bottom - 1);
				::Ellipse(hDC, rcParticle.left, rcParticle.top, rcParticle.right, rcParticle.bottom);
				//Drawing pie
				if (totalLocal > 0)
				{
					//Calculating pie
					auto angle = ceil(360.0f*static_cast<float>(totalLocal) / static_cast<float>(totalParticles));
					//Setting particle
					::BeginPath(hDC);
					::MoveToEx(hDC, centerX, centerY, nullptr);
					::AngleArc(hDC, centerX, centerY, radius, LastAngle, angle);
					::LineTo(hDC, centerX, centerY);
					::EndPath(hDC);
					StrokeAndFillPath(hDC);
					//Getting last data
					LastAngle += angle;
					line++;
				}
				//Returning old objects
				::SelectObject(hDC, oldbrush);
				::SelectObject(hDC, oldpen);
				//Cleaning
				::DeleteObject(brushparticle);
				::DeleteObject(penparticle);
			}
		});
		//Printing particles counter title
		oss.str("");
		rc.top = rc.bottom;		rc.bottom += size;
		oss << "Total: " << totalParticles;
		::DrawTextA(hDC, oss.str().c_str(), oss.str().size(), &rc, DT_LEFT | DT_VCENTER);
		//Drawing pie canvas
		auto oldPen = ::SelectObject(hDC, ::GetStockObject(WHITE_PEN));
		auto oldBrush = ::SelectObject(hDC, ::GetStockObject(HOLLOW_BRUSH));
		::Ellipse(hDC, centerX - radius, centerY - radius, centerX + radius, centerY + radius);
		::SelectObject(hDC, oldPen);
		::SelectObject(hDC, oldBrush);
	}
	//Drawing labels
	{
		auto rc = canvas;
		rc.bottom = size;
		//Printing Title
		oss.str("");
		oss << "Life Simulation";
		//::Rectangle(hDC,rc.left,rc.top,rc.right,rc.bottom);
		::DrawTextA(hDC, oss.str().c_str(), oss.str().size(), &rc, DT_CENTER | DT_VCENTER);
		//Printing particles counter title 
		rc.top = rc.bottom;
		rc.bottom += size;
		rc.top = rc.bottom;
		rc.bottom += size;
		oss.str("");
		oss << activeParticles << " - Particles (A,S,D)";
		::DrawTextA(hDC, oss.str().c_str(), oss.str().size(), &rc, DT_LEFT | DT_VCENTER);
		//Printing control label
		oss.str("");
		rc.top = rc.bottom = 22 * size;
		rc.bottom += size;
		oss << "Keyboard Controls:";
		::DrawTextA(hDC, oss.str().c_str(), oss.str().size(), &rc, DT_LEFT | DT_VCENTER);
		//Printing <space bar>
		oss.str("");
		rc.top = rc.bottom;	rc.bottom += size;
		oss << "<Space> Play/Pause";
		::DrawTextA(hDC, oss.str().c_str(), oss.str().size(), &rc, DT_LEFT | DT_VCENTER);
		//Printing <S>
		oss.str("");
		rc.top = rc.bottom;	rc.bottom += size;
		oss << "<S/s>: Save particles";
		::DrawTextA(hDC, oss.str().c_str(), oss.str().size(), &rc, DT_LEFT | DT_VCENTER);
		oss.str("");
		rc.top = rc.bottom;	rc.bottom += size;
		oss << "<M/m> Toggle Map";
		::DrawTextA(hDC, oss.str().c_str(), oss.str().size(), &rc, DT_LEFT | DT_VCENTER);
		//Printing <Turn>
		oss.str("");
		rc.top = rc.bottom;	rc.bottom += size;
		oss << "Turns: " << data->ViewStatistics().Turns;
		::DrawTextA(hDC, oss.str().c_str(), oss.str().size(), &rc, DT_LEFT | DT_VCENTER);
		//Printing <Replications>
		oss.str("");
		rc.top = rc.bottom;	rc.bottom += size;
		oss << "Repl: " << data->ViewStatistics().Replications;
		::DrawTextA(hDC, oss.str().c_str(), oss.str().size(), &rc, DT_LEFT | DT_VCENTER);
		//Printing <Deaths>
		oss.str("");
		rc.top = rc.bottom;	rc.bottom += size;
		oss << "Deaths: " << data->ViewStatistics().Deaths;
		::DrawTextA(hDC, oss.str().c_str(), oss.str().size(), &rc, DT_LEFT | DT_VCENTER);
	}
}

bool CEngine_View::Render(HDC& hDC, int width, int height, const IEngine_DataToView* data)
{
	auto ret = false;
	auto offsetY = static_cast<float>(height) / static_cast<float>(data->GetMapSize());
	auto offsetX = static_cast<float>(height) / static_cast<float>(data->GetMapSize());
	//MaxAttributes
	auto maxAttack = data->ViewStatistics().MaxAttack > 0 ? data->ViewStatistics().MaxAttack : 1;
	auto maxDefense = data->ViewStatistics().MaxDefense > 0 ? data->ViewStatistics().MaxDefense : 1;
	auto maxSpeed = data->ViewStatistics().MaxSpeed > 0 ? data->ViewStatistics().MaxSpeed : 1;
	auto maxEnergy = data->ViewConfig().EnergyMax;
	//Concurrency::critical_section cs;
	auto totalParticles = 0;
	//Rendering map
	std::for_each(data->ViewMap().begin(), data->ViewMap().end(), [&](const MAP_CELL& cell)
		//Concurrency::parallel_for_each(data.GetMap().begin(),data.GetMap().end(),[&] (const MAP_CELL& cell)
	{
		//Creating view objects
		HPEN	penMap = nullptr;//CreatePen(PS_SOLID,1,RGB(20,20,20));
		HBRUSH	brushMap = nullptr;//CreateSolidBrush(RGB(150*cell.Base/5,100*cell.Base/5,50*cell.Base/250));
		//Toggle colors
		if (m_bMapActive)
		{
			auto baseMax = data->ViewConfig().BaseMax;
			penMap = CreatePen(PS_SOLID, 1, RGB(20, 20, 20));
			brushMap = CreateSolidBrush(RGB(150 * cell.Base / baseMax, 100 * cell.Base / baseMax, 50 * cell.Base / baseMax));
		}
		else
		{
			penMap = CreatePen(PS_SOLID, 1, RGB(20, 20, 20));
			brushMap = CreateSolidBrush(RGB(30, 30, 30));
		}
		//Selecting view objects
		//cs.lock();
		auto oldbrushMap = SelectObject(hDC, brushMap);
		auto oldpenMap = SelectObject(hDC, penMap);
		//Drawing map base
		::Rectangle(
			hDC,
			static_cast<int>(cell.j*offsetX + 0),
			static_cast<int>(cell.i*offsetY + 0),
			static_cast<int>(cell.j*offsetX + offsetX),
			static_cast<int>(cell.i*offsetY + offsetY));
		//cs.unlock();
		//Returning old objects
		//cs.lock();
		::SelectObject(hDC, oldbrushMap);
		::SelectObject(hDC, oldpenMap);
		//cs.unlock();
		//Cleaning
		::DeleteObject(brushMap);
		::DeleteObject(penMap);
		if (cell.Particle != nullptr)
		{
			//Rendering particles
			auto attack = cell.Particle->GetAttributes().Attack;
			auto defense = cell.Particle->GetAttributes().Defense;
			auto speed = cell.Particle->GetAttributes().Speed;
			auto energy = cell.Particle->GetAttributes().Energy;
			auto DNAColorBrush = 0;
			auto DNAColorPen = 0;
			//Choosing particle color
			DNAColorPen = DNAColorBrush = lGetParticleColor(attack, speed, defense, maxAttack, maxSpeed, maxDefense, maxEnergy);
			//Creating view objects
			auto penParticle = CreatePen(PS_SOLID, 1, DNAColorPen);
			auto brushParticle = CreateSolidBrush(DNAColorBrush);
			//Selecting view objects
			//cs.lock();
			HGDIOBJ oldbrushParticle = SelectObject(hDC, brushParticle);
			HGDIOBJ oldpenParticle = SelectObject(hDC, penParticle);
			if (m_bMapActive)
			{
				auto RadiusX = static_cast<int>(0.5f*offsetX* (maxEnergy ? energy / maxEnergy : 1));
				auto RadiusY = static_cast<int>(0.5f*offsetY* (maxEnergy ? energy / maxEnergy : 1));
				//Drawing
				::Ellipse(
					hDC,
					static_cast<int>(cell.j*offsetX + 0.5f*offsetX - RadiusX),
					static_cast<int>(cell.i*offsetY + 0.5f*offsetY - RadiusY),
					static_cast<int>(cell.j*offsetX + 0.5f*offsetX + RadiusX),
					static_cast<int>(cell.i*offsetY + 0.5f*offsetY + RadiusY));
			}
			else
			{
				::Rectangle(
					hDC,
					static_cast<int>(cell.j*offsetX + 0.0f*offsetX + 0),
					static_cast<int>(cell.i*offsetY + 0.0f*offsetY + 0),
					static_cast<int>(cell.j*offsetX + 1.0f*offsetX - 0),
					static_cast<int>(cell.i*offsetY + 1.0f*offsetY - 0));
			}
			//Returning old objects
			::SelectObject(hDC, oldbrushParticle);
			::SelectObject(hDC, oldpenParticle);
			//cs.unlock();
			//Cleaning
			::DeleteObject(brushParticle);
			::DeleteObject(penParticle);
			ret = true;
			//Calculating statistics
			totalParticles++;
		}
	});
	//Drawing summary
	DrawSummary(hDC,width, height, data, totalParticles);

	return ret;
}