#pragma once
#include <string>
#include "Engine_Data.h"
class CEngine_Processor
{
public:
	CEngine_Processor(void);
	virtual ~CEngine_Processor(void);
	//Initialize & Finalize
	virtual bool Initialize(std::string& error);
	virtual bool Finalize(std::string& error);
	//Processing
	bool DoTurn(CEngine_Data& data,long time);
	bool DoStatistics(CEngine_Data& data);
};