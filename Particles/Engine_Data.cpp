#include "Engine_Data.h"
#include <list>
#include <algorithm>
#include <sstream>

typedef std::vector<std::string>	CSVFILE_LINE;
typedef std::vector<CSVFILE_LINE>	CSVFILE_LINES;
#define		MAP_FILENAME			"Map.csv"
#define		PARTICLES_FILENAME		"Particles.csv"
#define		CONFIG_FILENAME			"Config.csv"
#define		STATISTICS_FILENAME		"Statistics.csv"

#define SAFE_DELETE(p) {if(p != nullptr){ delete p; p = nullptr;}}

template<typename T>
std::string ToString(const T& t)
{
	std::ostringstream s;
	s << t;
	return s.str();
}

template<typename T>
T FromString(const std::string& s)
{
	std::istringstream is(s);
	T t;
	is >> t;
	return t;
}

CEngine_Data::CEngine_Data(void) :
	m_nMapSize(0)
{
	memset(&m_Config,0,sizeof(m_Config));
	//Default for config
	m_Config.LifePenalty = 500;
	m_Config.EnergyMax  = 100.0f;
	m_Config.BaseMax = 20.0f;
	m_Config.BaseFactor = 1.0f;
}

CEngine_Data::~CEngine_Data(void)
{
}

void lSplitByToken(std::string inStr, std::list<std::string> &outList, char token)
{
	if(inStr.empty())
	{
		return;
	}
	std::size_t size = inStr.size();
	std::size_t offset = 0;
	std::size_t pos = 0;

	while (offset < size)
	{
		pos = inStr.find(token,offset);
		if (std::string::npos == pos)
		{
			pos = size;
		}
		if (pos > offset)
		{
			auto param = inStr.substr(offset, (pos - offset));
			if(!param.empty())
			{
				outList.push_back(param);
			}
			offset = pos + 1;
		}
	}
}

bool lSaveDataToFile(const char* filename,const std::string& buffer)
{
	auto ret = true;
	//Opening file
	auto file = fopen(filename,"w");
	if(file == nullptr)
	{
		ret = false;
	}
	else
	{
		fwrite(buffer.c_str(),buffer.size(),1,file);
	}
	//closing file
	fclose(file);

	return ret;
}

bool lReadDataFromFile(const char* filename,std::vector<char>& buffer)
{
	bool ret = true;
	//Opening file
	FILE* file = fopen(filename,"r+");
	if(file == nullptr)
	{
		ret = false;
	}
	else
	{
		//Reading data
		buffer.resize(0);
		char	line[4096]	= {""};
		int		BytesRead	= 0;
		int		pos			= 0;
		while((BytesRead = fread(line,sizeof(char),sizeof(line) - 1,file)) > 0)
		{
			buffer.resize(buffer.size() + BytesRead);
			memcpy(&buffer[pos],line,BytesRead);
			pos += BytesRead;
		}
		//closing file
		fclose(file);
	}

	return ret;
}

bool lLoadCSVFile(const char* filename,CSVFILE_LINES& lines)
{
	bool				ret = true;
	std::vector<char>	buffer;
	std::list<std::string> l;
	//Loading from file
	if(!(ret = lReadDataFromFile(filename,buffer)) || buffer.size() == 0)
	{
		ret = false;
		goto exit;
	}
	//testing the last char
	if(buffer[buffer.size() - 1] == '\n')
	{
		buffer[buffer.size() - 1] = 0;
	}
	buffer.push_back('\0');
	//Reading content
	lSplitByToken(&buffer[0],l,'\n');
	//Setting data
	std::for_each(l.begin(),l.end(),[&lines] (std::string& strLine)
	{
		CSVFILE_LINE line;
		std::list<std::string> l;
		lSplitByToken(strLine,l,';');
		std::for_each(l.begin(),l.end(),[&line](std::string& item)
		{
			line.push_back(item);
		});
		lines.push_back(line);
	});

exit:
	return ret;
}

bool lSettingMap(CSVFILE_LINES& lines,MAP& map,int& nMapSize)
{
	bool ret = true;
	int i = 0;
	std::for_each(lines.begin(),lines.end(),[&](CSVFILE_LINE& line) mutable
	{
		int j = 0;
		std::for_each(line.begin(),line.end(),[&](std::string& base) mutable
		{
			MAP_CELL cell = {0};
			cell.i = i;
			cell.j = j;
			cell.Base = (base.size() > 0 ? FromString<float>(base) : 0);
			map.push_back(cell);
			j++;
		});
		i++;
	});
	nMapSize = i;

	return ret;
}

bool CEngine_Data::SetDefaultMap()
{
	bool ret = true;
	int i = 0;
	int j = 0;
	//Setting map size
	m_nMapSize = 100;
	//Setting vector
	m_map.resize(m_nMapSize*m_nMapSize);
	//setting map
	std::for_each(m_map.begin(),m_map.end(),[&](MAP_CELL& cell)
	{
		//Setting values
		cell.i = i;
		cell.j = j;
		cell.Base = 0;//static_cast<float>(rand() % 6);
		cell.Particle = nullptr;
		//Testing rows x collumns
		if(j < m_nMapSize - 1)
		{
			j++;
		}
		else
		{
			j = 0;
			i++;
		}
	});

	return ret;
}

bool CEngine_Data::SetDefaultParticles()
{
	auto ret = true;
	auto& cell	= GetCell(m_nMapSize/2,m_nMapSize/2);
	cell.Particle	= new Particle();
	//Name;attack;defense;speed;energy;replicated;x;y
	cell.Particle->SetName(std::string("1"));
	cell.Particle->GetAttributes().Attack.SetMin();
	cell.Particle->GetAttributes().Speed.SetMin();
	cell.Particle->GetAttributes().AccmulatedSpeed.SetMin();
	cell.Particle->GetAttributes().Defense.SetMin();
	cell.Particle->GetAttributes().Energy.SetValue(m_Config.EnergyMax/2);
	cell.Particle->GetAttributes().Replicated	= 1;
	cell.Particle->GetAttributes().Turns		= 0;

	return ret;
}

bool CEngine_Data::SaveMap(std::string& error)
{
	bool ret = true;
	int i = 0;
	int j = 0;
	std::string buffer;
	std::ostringstream oss;
	std::for_each(m_map.begin(),m_map.end(),[&](MAP_CELL& cell)
	{
		oss << cell.Base;
		if(j < m_nMapSize - 1)
		{
			j++;
			oss << ";";
		}
		else
		{
			j = 0;
			i++;
			oss << std::endl;
		}
	});
	//Remving last carriage return
	buffer = oss.str().substr(0,oss.str().size() - 1);
	//Saving to file
	if(!(ret = lSaveDataToFile(MAP_FILENAME,buffer)))
	{
		error = "Error on persist map to file...";
		ret = false;
	}

	return ret;
}

bool CEngine_Data::Initialize(std::string& error)
{
	bool ret = true;
	//Loading map file
	if(!(ret = LoadMap(error)))
	{
		if(!(ret = SetDefaultMap()))
		{
			goto exit;
		}
		if(!(ret = SaveMap(error)))
		{
			goto exit;
		}
	}
	//Loading config
	if (!(ret = LoadConfig(error)))
	{
		if (!(ret = SaveConfig(error)))
		{
			goto exit;
		}
	}
	//Loading particles
	if(!(ret = LoadParticles(error)))
	{
		if(!(ret = SetDefaultParticles()))
		{
			goto exit;
		}
		if(!(ret = SaveParticles(error)))
		{
			goto exit;
		}
	}
	//Loading Statistics
	if(!(ret = LoadStatistics(error)))
	{
		if(!(ret = SaveStatistics(error)))
		{
			goto exit;
		}
	}

exit:
	return ret;
}

bool CEngine_Data::Finalize(std::string& error)
{
	bool ret = true;
	//Cleaning map
	std::for_each(m_map.begin(),m_map.end(),[&](MAP_CELL& cell)
	{
		SAFE_DELETE(cell.Particle);
	});

	return ret;
}

bool CEngine_Data::LoadMap(std::string& error)
{
	bool ret = true;
	CSVFILE_LINES lines;
	if(!(ret = lLoadCSVFile(MAP_FILENAME,lines)))
	{
		error = "Error loading map...";
		goto exit;
	}
	//Setting map
	if(!(ret = lSettingMap(lines,m_map,m_nMapSize)))
	{
		error = "Error on setting map...";
		goto exit;
	}

exit:
	return ret;
}

bool lSettingParticles(CSVFILE_LINES& lines,MAP& map,int& nSize)
{
	auto ret = true;
	if (lines.size() > 1)
	{
		auto it = lines.begin();
		std::for_each(++it, lines.end(), [&map, nSize](CSVFILE_LINE& line)
		{
			auto i = 0;
			auto j = 0;
			auto particle = new Particle();
			//Name;attack;speed;defense;energy;replicated;x;y;accumulatedSpeed
			particle->SetName(line[0]);
			particle->GetAttributes().Attack.SetValue(FromString<float>(line[1]));
			particle->GetAttributes().Speed.SetValue(FromString<float>(line[2]));
			particle->GetAttributes().Defense.SetValue(FromString<float>(line[3]));
			particle->GetAttributes().Energy.SetValue(FromString<float>(line[4]));
			particle->GetAttributes().Replicated = FromString<int>(line[5]);
			i = FromString<int>(line[6]);
			j = FromString<int>(line[7]);
			particle->GetAttributes().Turns = FromString<int>(line[8]);
			particle->GetAttributes().AccmulatedSpeed.SetValue(FromString<float>(line[9]));
			//cleaning if needed
			SAFE_DELETE(map[i*nSize + j].Particle);
			//assign the new particle
			map[i*nSize + j].Particle = particle;
		});
	}

	return ret;
}

bool CEngine_Data::LoadParticles(std::string& error)
{
	bool ret = true;
	CSVFILE_LINES lines;
	if(!(ret = lLoadCSVFile(PARTICLES_FILENAME,lines)))
	{
		error = "Error loading particles...";
		goto exit;
	}
	//Setting particles
	if(!(ret = lSettingParticles(lines,m_map,m_nMapSize)))
	{
		error = "Error on setting particles...";
		goto exit;
	}

exit:
	return ret;
}

bool CEngine_Data::SaveParticles(std::string& error)
{
	bool ret = true;
	std::string buffer;
	std::ostringstream oss;
	//Setting header
	oss << "Name;Attack;Speed;Defense;Energy;Replicated;i;j;Turns;accumulatedSpeed" << std::endl;
	//Saving particles
	std::for_each(m_map.begin(),m_map.end(),[&](MAP_CELL& cell)
	{
		if(cell.Particle != nullptr)
		{
			oss << cell.Particle->GetName() << ";";
			oss << cell.Particle->GetAttributes().Attack << ";";
			oss << cell.Particle->GetAttributes().Speed << ";";
			oss << cell.Particle->GetAttributes().Defense << ";";
			oss << cell.Particle->GetAttributes().Energy << ";";
			oss << cell.Particle->GetAttributes().Replicated << ";";
			oss << cell.i << ";";
			oss << cell.j << ";";
			oss << cell.Particle->GetAttributes().Turns << ";";
			oss << cell.Particle->GetAttributes().AccmulatedSpeed << std::endl;
		}
	});
	//Remving last carriage return
	buffer = oss.str().substr(0,oss.str().size() - 1);
	//Saving to file
	if(!(ret = lSaveDataToFile(PARTICLES_FILENAME,buffer)))
	{
		error = "Error on persist data to file...";
		ret = false;
	}

	return ret;
}

bool CEngine_Data::PersistData(std::string& error)
{
	bool ret = true;
	//Saving particles
	if(!SaveParticles(error))
	{
		ret = false;
		goto exit;
	}
	//Saving config
	if(!SaveConfig(error))
	{
		ret = false;
		goto exit;
	}

	//Saving statistics
	if(!SaveStatistics(error))
	{
		ret = false;
		goto exit;
	}

exit:
	return ret;
}

static bool lSettingConfig(CSVFILE_LINES& lines,CONFIG& config)
{
	bool ret = true;
	//Getting data
	auto it = lines.begin();
	if(it != lines.end())
	{
		++it;
		if (it != lines.end())
		{
			auto& line = *it;
			config.LifePenalty = FromString<int>(line[0]);
			config.EnergyMax = FromString<float>(line[1]);
			config.BaseMax = FromString<float>(line[2]);
			config.BaseFactor = FromString<float>(line[3]);
		}
	}
	else
	{
		ret = false;
	}

	return ret;
}

bool CEngine_Data::LoadConfig(std::string& error)
{
	bool ret = true;
	CSVFILE_LINES lines;
	if(!(ret = lLoadCSVFile(CONFIG_FILENAME,lines)))
	{
		error = "Error loading config...";
		goto exit;
	}
	//Setting particles
	if(!(ret = lSettingConfig(lines,m_Config)))
	{
		error = "Error on setting config...";
		goto exit;
	}

exit:
	return ret;
}

bool CEngine_Data::SaveConfig(std::string& error)
{
	bool ret = true;
	std::string buffer;
	std::ostringstream oss;
	//Saving config
	oss << "LifePenalty;EnergyMax;BaseMax;BaseFactor" << std::endl;
	oss << m_Config.LifePenalty << ";";
	oss << m_Config.EnergyMax << ";";
	oss << m_Config.BaseMax << ";";
	oss << m_Config.BaseFactor;
	buffer = oss.str();
	//Saving to file
	if(!(ret = lSaveDataToFile(CONFIG_FILENAME,buffer)))
	{
		error = "Error on persist data to file...";
		ret = false;
	}

	return ret;
}

MAP_CELL& CEngine_Data::GetCell(int i,int j)
{
	return m_map[i*m_nMapSize + j];
}

bool lSettingStatistics(CSVFILE_LINES& lines, STATISTICS& Statistics)
{
	auto ret = true;
	auto it = lines.begin();
	if (it != lines.end())
	{
		auto l = *it;
		//Getting turns
		auto turns = l[0];
		Statistics.Turns = FromString<int>(turns);
		//Getting replications
		if (l.size() > 1)
		{
			auto replications = l[1];
			Statistics.Replications = FromString<int>(replications);
		}
		//Getting deaths
		if (l.size() > 2)
		{
			auto deaths = l[2];
			Statistics.Deaths = FromString<int>(deaths);
		}
		++it;
		//Getting particles statistics
		std::for_each(it, lines.end(), [&Statistics](CSVFILE_LINE& line)
		{
			PARTICLE_STATISTIC s;
			s.Count = FromString<int>(line[0]);
			s.Turns = FromString<int>(line[1]);
			s.Attack = FromString<float>(line[2]);
			s.Speed = FromString<float>(line[3]);
			s.Defense = FromString<float>(line[4]);
			Statistics.Particles.push_back(s);
		});
	}
	else
	{
		ret = false;
	}

	return ret;
}

bool CEngine_Data::LoadStatistics(std::string& error)
{
	bool ret = true;
	CSVFILE_LINES lines;
	if(!(ret = lLoadCSVFile(STATISTICS_FILENAME,lines)))
	{
		error = "Error loading statistics...";
		goto exit;
	}
	//Setting Statistics
	if(!(ret = lSettingStatistics(lines,m_Statistics)))
	{
		error = "Error on setting statistics...";
		goto exit;
	}

exit:
	return ret;
}

bool CEngine_Data::SaveStatistics(std::string& error)
{
	bool ret = true;
	//Ordering statistics
	std::sort(m_Statistics.Particles.begin(),m_Statistics.Particles.end(),[&](const PARTICLE_STATISTIC& s1,const PARTICLE_STATISTIC& s2)
	{
		return (s1.Turns > s2.Turns);
	});
	//Saving statistics
	std::string buffer;
	std::ostringstream oss;
	//Saving turns
	oss << m_Statistics.Turns << ";";
	oss << m_Statistics.Replications << ";";
	oss << m_Statistics.Deaths << std::endl;
	std::for_each(m_Statistics.Particles.begin(),m_Statistics.Particles.end(),[&](PARTICLE_STATISTIC& s)
	{
		oss << s.Count << ";";
		oss << s.Turns << ";";
		oss << s.Attack << ";";
		oss << s.Speed << ";";
		oss << s.Defense << std::endl;
	});
	//Remving last carriage return
	buffer = oss.str().substr(0,oss.str().size() - 1);
	//Saving to file
	if(!(ret = lSaveDataToFile(STATISTICS_FILENAME,buffer)))
	{
		error = "Error on persist data to file...";
		ret = false;
	}

	return ret;
}

void CEngine_Data::GenerateReport(std::string& report)
{
	int nCount = 0;
	//Saving statistics
	std::string error;
	SaveStatistics(error);
	//Printing report
	std::ostringstream oss;
	std::for_each(m_Statistics.Particles.begin(),m_Statistics.Particles.end(),[&](const PARTICLE_STATISTIC& s)
	{
		if(nCount++ < 5)
		{
			oss << "[" << s.Attack << "," << s.Speed << "," << s.Defense << "] = " << s.Turns << std::endl;
		}
	});
	oss << "Types of particles = " << m_Statistics.Particles.size() << std::endl;
	oss << "The simulation ran " << m_Statistics.Turns << " times" << std::endl;
	oss << "Replications " << m_Statistics.Replications << std::endl;
	oss << "Deaths " << m_Statistics.Deaths;
	report = oss.str();
}