#pragma once
#include <string>
#include <vector>
#include "Particle.h"
#include <concurrent_vector.h>
#include "IEngine_DataToView.h"

//Particles
//Name;attack;defense;speed;energy;replicated;x;y;Turns
//1;0;0;0;50;1;25;25;0

//Config
//Turns;LifePenalty
//0;100

//Statistics
//nTurns;attack;speed;defense

class CEngine_Data : public IEngine_DataToView
{
private:
	MAP					m_map;
	int					m_nMapSize;
	CONFIG				m_Config;
	STATISTICS			m_Statistics;
	//Map
	bool SaveMap(std::string& error);
	bool LoadMap(std::string& error);
	bool SetDefaultMap();
	//Particles
	bool LoadParticles(std::string& error);
	bool SaveParticles(std::string& error);
	bool SetDefaultParticles();
	//Config
	bool LoadConfig(std::string& error);
	bool SaveConfig(std::string& error);
	//Statistics
	bool LoadStatistics(std::string& error);
	bool SaveStatistics(std::string& error);
public:
	CEngine_Data(void);
	~CEngine_Data(void);
	//Initialize & Finalize
	virtual bool Initialize(std::string& error);
	virtual bool Finalize(std::string& error);
	//Get
	const CONFIG&	GetConfig()				{return m_Config;}
	MAP&			GetMap()				{return m_map;}
	STATISTICS&		GetStatistics()			{return m_Statistics;}
	MAP_CELL&		GetCell(int i,int j);
	//Operation
	bool PersistData(std::string& error);
	void IncrementTurn()					{m_Statistics.Turns++;}
	void GenerateReport(std::string& report);
	//IEngine_DataToview
	virtual const CONFIG&		ViewConfig() const		{return m_Config;}
	virtual const MAP&			ViewMap() const			{return m_map;}
	virtual const STATISTICS&	ViewStatistics() const	{return m_Statistics;}
	virtual const int			GetMapSize() const		{return m_nMapSize;}
};