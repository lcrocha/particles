#Particles
- This project simulates simple organism life with biological interactions such Autotrophism, Heterotrophism, etc.

#Keywords:
- Visual C++ 2013
- C++ 11
- Lambdas
- MVC
- GDI+
- Double Buffer Graphics
- STL
- AMP
- PPL